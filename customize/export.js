// Use this file to customize what the library exports

// Whatever you set the EXPORT variable to, is what the global variable named "<MODULE_NAME>" 
// (the same MODULE_NAME from coldbrew_settings.py) will be set to in browsers or what 
// will be exported in Node.js when require()-ing this library.

// If you leave EXPORT set to null, the entire Coldbrew Python environment will be exported by default.

EXPORT = null;